/**
 * 
 * $Id: $
 *
 * this file contains the defines that convert our x-macros to datatypes
 */

#undef SUBSTRUCT
#undef SUBSTRUCT_ELEMENT
#undef CFG_VALUE
#undef PROTOCOL_ONLY
#undef SERVER_PRIVATE
#undef NO_ARTV

#undef UNSIGNED
#undef UNSIGNED_INT
#undef LONG
#undef INTEGER
#undef UINT8
#undef UNSIGNED_SHORT
#undef CHAR      

#undef TIME
#undef UID_T    

#undef STRING_BUF
#undef STRING



