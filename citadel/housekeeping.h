/* $Id$ */
void terminate_idle_sessions(void);
void check_sched_shutdown(void);
void check_ref_counts(void);
void do_housekeeping(void);
