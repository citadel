/* $Id$ */
void get_control (void);
void put_control (void);
void release_control(void);
void check_control(void);
long int get_new_message_number (void);
long int get_new_user_number (void);
long int get_new_room_number (void);
void cmd_conf(char *argbuf);
