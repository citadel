/*
 * $Id$
 *
 */

#ifdef HAVE_LDAP

void ctdl_vcard_to_ldap(struct CtdlMessage *msg, int op);

#endif /* HAVE_LDAP */
