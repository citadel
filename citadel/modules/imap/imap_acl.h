/*
 * $Id$
 *
 */

void imap_setacl(int num_parms, char *parms[]);
void imap_deleteacl(int num_parms, char *parms[]);
void imap_getacl(int num_parms, char *parms[]);
void imap_listrights(int num_parms, char *parms[]);
void imap_myrights(int num_parms, char *parms[]);

