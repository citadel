#!/bin/sh
#
# Script to generate $C_FILE
#

ECHO=/usr/bin/printf

#MINUS_e=X`$ECHO -n -e`
#if [ $MINUS_e != "X" ] ; then
#	MINUS_e=""
#else
#	MINUS_e="-e"
#fi

#MINUS_E=X`$ECHO -n -E`
#if [ $MINUS_E != "X" ] ; then
#	MINUS_E=""
#else
#	MINUS_E="-E"
#fi


CUR_DIR=`pwd`
C_FILE="$CUR_DIR/modules_init.c"
H_FILE="$CUR_DIR/modules_init.h"
MOD_FILE="$CUR_DIR/Make_modules"
SRC_FILE="$CUR_DIR/Make_sources"
U_FILE="$CUR_DIR/modules_upgrade.c"

/usr/bin/printf "Scanning extension modules for entry points.\n"


#start of the files which inturn removes any existing file
#

# start the Makefile included file for $SERV_MODULES
cat <<EOF  >$MOD_FILE
#
# Make_modules
# This file is to be included by Makefile to dynamically add modules to the build process
# THIS FILE WAS AUTO GENERATED BY mk_modules_init.sh DO NOT EDIT THIS FILE
#

EOF

# start the Makefile included file for $SOURCES
cat <<EOF  >$SRC_FILE
#
# Make_sources
# This file is to be included by Makefile to dynamically add modules to the build process
# THIS FILE WAS AUTO GENERATED BY mk_modules_init.sh DO NOT EDIT THIS FILE
#

EOF

# start the c file
cat <<EOF  >$C_FILE
/*
 * $C_FILE
 * Auto generated by mk_modules_init.sh DO NOT EDIT THIS FILE
 */



#include "sysdep.h"
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <libcitadel.h>
#include "webcit.h"
#include "modules_init.h"
#include "webserver.h"

void LogPrintMessages(long err);
extern long DetailErrorFlags;



void initialise_modules (void)
{

EOF


#start the header file
cat <<EOF > $H_FILE
/* 
 * $H_FILE
 * Auto generated by mk_modules_init.sh DO NOT EDIT THIS FILE
 */


#ifndef MODULES_INIT_H
#define MODULES_INIT_H
extern size_t nSizErrmsg;
void initialise_modules (void);


EOF


INIT_FUNCS=`grep InitModule_ *.c |sed "s;.*:;;"`

for HOOK in $INIT_FUNCS; do
HOOKNAME=`echo $HOOK |sed "s;InitModule_;;"`
# Add this entry point to the .c file
cat <<EOF >> $C_FILE
#ifdef DBG_PRINNT_HOOKS_AT_START
	lprintf (CTDL_INFO, "Initializing $HOOKNAME\n");
#endif
	$HOOK();
EOF
# Add this entry point to the .h file
cat <<EOF >> $H_FILE
	extern void $HOOK(void);
EOF
done


/usr/bin/printf "}\n" >> $C_FILE

/usr/bin/printf "\n#endif /* MODULES_INIT_H */\n" >> $H_FILE
